const send = require('./src/actions/send')
const subscriptions = require('./src/actions/subscriptions')

exports.send = (event, context, cb) => {
  send().then(
    () => cb(null, { statusCode: 204 }),
    err => {
      console.error(err)
      cb(err)
    }
  )
}

exports.message = (event, context, cb) => {
  const payload = event.body ? JSON.parse(event.body) : undefined
  subscriptions(payload).then(
    response => cb(null, { statusCode: 200, body: JSON.stringify(response) }),
    cb
  )
}
