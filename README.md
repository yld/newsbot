# Coko Newsbot

A Mattermost newsletter bot built on serverless infrastructure

## Set up

1. [Set up AWS credentials][1]
2. `cp config.sample.json config.json`
3. [Get a Mattermost token][2] and save this is the config
4. `npm install`
5. `npx serverless deploy`
6. Copy the URL for the `message` function and create an Outgoing Webhook in Mattermost

[1]: https://serverless.com/framework/docs/providers/aws/guide/credentials/
[2]: https://api.mattermost.com/#tag/authentication

## How it works
 
- There's an outgoing webhook in Mattermost which calls the `message` Lambda function.
- Users can subscribe/unsubscribe from emails by saying `[trigger word] subscribe` or 
  `[trigger word] unsubscribe` where `[trigger word]` is whatever was configured in the 
  webhook.
- The subscriber list is stored as a user preference against the API user (i.e. the 
  Mattermost user whose session token is configured in `config.json`).
- The `send` function is triggered on a schedule as defined in `serverless.yml`.
    - Loads all posts from the channels to which the API user belongs and 
      checks them for key words and reactions as defined in `src/actions/send.js`.
    - Renders posts into HTML email template.
    - Posts email to Amazon SES.  

## Development tips

- There are two *stages*: `development` (default) and `production`.
  - For testing, it is useful to create a second webhook which hits the endpoint for the 
    `development` stage
- View logs for a function with `npx serverless logs -tf [function]`
- Run a function locally with `npx serverless invoke local -f [function]`
- Deploy a single function (faster) `npx serverless deploy function -f [function]`
- Deploy to production `npx serverless deploy -s production`