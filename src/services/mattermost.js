const request = require('request-promise-native')
const config = require('../config')

const mattermost = request.defaults({
  auth: {
    bearer: config.mattermostSessionToken,
  },
  json: true,
})

module.exports = {
  get(path, qs = {}) {
    console.log('GET', path, qs)
    return mattermost.get(config.mattermostUrl + path, { qs })
  },
  post(path, body = {}) {
    console.log('POST', path, body)
    return mattermost.post(config.mattermostUrl + path, { body })
  },
  put(path, body = {}) {
    console.log('PUT', path, body)
    return mattermost.put(config.mattermostUrl + path, { body })
  },
}
