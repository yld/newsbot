jest.mock('./mattermost')

const newsletter = require('./newsletter')
const mattermost = require('./mattermost')

describe('Newsletter service', () => {
  beforeEach(() => jest.clearAllMocks())

  it('subscribes a user', async () => {
    mattermost.get
      .mockReturnValueOnce(Promise.resolve({ value: '["123abc","456def"]' }))
      .mockReturnValueOnce({ id: '789ghi' })

    await newsletter.subscribe('000aaa')
    expect(mattermost.put).toHaveBeenCalledWith('users/me/preferences', [
      {
        user_id: '789ghi',
        category: undefined,
        name: 'subscribers',
        value: '["123abc","456def","000aaa"]',
      },
    ])
  })

  it('unsubscribes a user', async () => {
    mattermost.get
      .mockReturnValueOnce(
        Promise.resolve({ value: '["123abc","456def","000aaa"]' })
      )
      .mockReturnValueOnce({ id: '789ghi' })

    await newsletter.unsubscribe('000aaa')
    expect(mattermost.put).toHaveBeenCalledWith('users/me/preferences', [
      {
        user_id: '789ghi',
        category: undefined,
        name: 'subscribers',
        value: '["123abc","456def"]',
      },
    ])
  })

  it('gets emails from users', async () => {
    mattermost.get.mockReturnValueOnce(
      Promise.resolve({ value: '["123abc","456def"]' })
    )
    mattermost.post.mockReturnValueOnce(
      Promise.resolve([
        { id: '123abc', email: 'abc@example.com' },
        { id: '456def', email: 'def@example.com' },
      ])
    )

    const emails = await newsletter.subscriberEmails()
    expect(mattermost.post).toHaveBeenCalledWith('users/ids', [
      '123abc',
      '456def',
    ])
    expect(emails).toEqual(['abc@example.com', 'def@example.com'])
  })
})
