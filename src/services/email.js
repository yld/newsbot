const nodeSes = require('node-ses')

const client = nodeSes.createClient({
  amazon: 'https://email.eu-west-1.amazonaws.com',
})

module.exports = function sendEmail(args) {
  return new Promise((resolve, reject) => {
    client.sendEmail(args, err => (err ? reject(err) : resolve()))
  })
}
