const union = require('lodash/union')
const without = require('lodash/without')
const mattermost = require('../services/mattermost')

// Uses the API user's preferences to store a
// serialised array of the current subscriber list

// this is set in serverless.yml and differs between development and production
const categoryName = process.env.NEWSBOT_PREF_CATEGORY

async function subscribe(userId) {
  return save(union(await load(), [userId]))
}

async function unsubscribe(userId) {
  return save(without(await load(), userId))
}

async function load() {
  try {
    const pref = await mattermost.get(
      `users/me/preferences/${categoryName}/name/subscribers`
    )
    return JSON.parse(pref.value)
  } catch (err) {
    if ([400, 404].includes(err.statusCode)) {
      return []
    }
    throw err
  }
}

async function save(userIds) {
  const self = await mattermost.get('users/me')
  const pref = {
    user_id: self.id,
    category: categoryName,
    name: 'subscribers',
    value: JSON.stringify(userIds),
  }
  return mattermost.put('users/me/preferences', [pref])
}

async function subscriberEmails() {
  const subscriberIds = await load()
  if (!subscriberIds.length) {
    return []
  }

  const users = await mattermost.post('users/ids', subscriberIds)
  return users.map(user => user.email)
}

module.exports = {
  subscribe,
  unsubscribe,
  load,
  save,
  subscriberEmails,
}
