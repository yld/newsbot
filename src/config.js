// try to load config file
try {
  module.exports = require('../config')
} catch (e) {
  throw new Error('Missing config. Please copy config.sample.json to config.json.')
}