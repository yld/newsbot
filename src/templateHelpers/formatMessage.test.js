import { markdown, emoji } from './formatMessage'

describe('formatMessage helper', () => {
  describe('markdown formatter', () => {
    it('renders markdown', () => {
      expect(markdown('Hello *there* `mate`')).toBe(
        'Hello <em>there</em> <code>mate</code>'
      )
    })

    it('escapes HTML', () => {
      expect(markdown('No <script>')).toBe('No &lt;script&gt;')
    })
  })

  describe('emoji converter', () => {
    it('converts emoji names to unicode', () => {
      expect(emoji(':+1: :slightly_smiling_face:')).toBe('👍 🙂')
    })

    it('ignores custom and unknown emoji names', () => {
      expect(emoji(':cokobot: :foobar:')).toBe(':cokobot: :foobar:')
    })
  })
})
