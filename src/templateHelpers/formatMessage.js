import Handlebars from 'handlebars'
import marked from 'marked'
import emojiNameMap from 'emoji-name-map'

// use inlineLexer to avoid wrapping output in <p>
export const markdown = text => marked.inlineLexer(text, [], { sanitize: true })

// convert :emoji: to unicode
export const emoji = text =>
  text.replace(/:([\w+]+):/g, match => emojiNameMap.get(match) || match)

export default text => new Handlebars.SafeString(markdown(emoji(text)))
