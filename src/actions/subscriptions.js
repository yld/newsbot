const newsletter = require('../services/newsletter')

const subscribeRegex = /\b(subscribe|add|start)\b/i
const unsubscribeRegex = /\b(unsubscribe|remove|stop)\b/i

module.exports = async payload => {
  console.log('Processing payload', payload)
  let message
  if (!payload || !payload.text) {
    message = `I'm not reading you. There's a blockage in the intertubes.`
  } else if (subscribeRegex.test(payload.text)) {
    await newsletter.subscribe(payload.user_id)
    console.log(`Subscribed ${payload.user_name}`)
    message = "You're on the list now"
  } else if (unsubscribeRegex.test(payload.text)) {
    await newsletter.unsubscribe(payload.user_id)
    console.log(`Unsubscribed ${payload.user_name}`)
    message = 'Sorry to see you go'
  } else {
    message =
      "Sorry, I didn't understand that. Try `subscribe` or `unsubscribe`."
  }

  return {
    icon_url:
      'https://gitlab.coko.foundation/yld/newsbot/uploads/33fadbd49df10317e42a5d8126e1b40e/cokobotSign.png',
    username: 'newsbot',
    text: message,
  }
}
