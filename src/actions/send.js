const { map, filter, forEach } = require('p-iteration')
const moment = require('moment')

const emailTemplate = require('./template.hbs')
const mattermost = require('../services/mattermost')
const newsletter = require('../services/newsletter')
const sendEmail = require('../services/email')
const config = require('../config')

// Configurable behaviour
const since = moment()
  .subtract(1, 'week')
  .valueOf()
const search = new RegExp(config.triggerPattern)
const emoji = config.triggerReaction

// load all posts in date range and filter for inclusion requirements
async function getPostsForChannel(channel) {
  const responseBody = await mattermost.get(`channels/${channel.id}/posts`, {
    since,
  })

  return filter(Object.values(responseBody.posts), async post => {
    if (post.delete_at) {
      // ignore deleted (or edited) posts
      return false
    }

    if (post.has_reactions) {
      // mutate post object!
      post.reactions = await mattermost.get(`posts/${post.id}/reactions`)
    }

    return (
      post.message.match(search) ||
      (post.reactions &&
        post.reactions.filter(reaction => reaction.emoji_name === emoji).length)
    )
  })
}

// get posts for all channels, sort and decorate with user and channel info
async function getPosts() {
  // fetch users channels (can't get posts from channels user hasn't joined)
  let channels = await mattermost.get(
    `users/me/teams/${config.mattermostTeamId}/channels`
  )

  // filter out private channels, direct message chats etc
  const openChannels = channels.filter(channel => channel.type === 'O')

  // get posts for each channel that match the criteria
  const postsPerChannel = await map(openChannels, getPostsForChannel)

  const includedPosts = postsPerChannel.reduce(
    (all, posts) => all.concat(posts),
    []
  )

  includedPosts.sort((a, b) => a.create_at - b.create_at)

  const userIds = includedPosts.map(post => post.user_id)
  const users = await mattermost.post('users/ids', userIds)

  const usersById = users.reduce(
    (obj, user) => ({ ...obj, [user.id]: user }),
    {}
  )
  const channelsById = channels.reduce(
    (obj, channel) => ({ ...obj, [channel.id]: channel }),
    {}
  )

  return includedPosts.map(post => ({
    ...post,
    display_date: moment(post.create_at).format('ddd Do MMM'),
    user: usersById[post.user_id],
    channel: channelsById[post.channel_id],
  }))
}

// render posts into email template
function renderEmail(posts) {
  return emailTemplate({ posts })
}

module.exports = async function main() {
  const posts = await getPosts()
  if (!posts.length) {
    console.log('No posts, quitting.')
    return
  }

  const recipients = await newsletter.subscriberEmails()
  if (!recipients.length) {
    console.log('No subscribers, quitting.')
    return
  }

  const emailBody = renderEmail(posts)

  await forEach(recipients, async recipient => {
    try {
      await sendEmail({
        from: config.emailFrom,
        to: recipient,
        subject: config.emailSubject,
        message: emailBody,
      })
      console.log(`Sent email to ${recipient}`)
    } catch (error) {
      console.log(`Failed to send to ${recipient}`, error)
    }
  })

  console.log(`Finished`)
}
