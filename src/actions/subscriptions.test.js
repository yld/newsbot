jest.mock('../services/newsletter')

const newsletter = require('../services/newsletter')
const subscriptions = require('./subscriptions')

describe('Subscription API endpoint', () => {
  beforeEach(() => jest.clearAllMocks())

  it('handles empty payload', async () => {
    const response = await subscriptions({})
    expect(response.text).toContain('blockage')
  })

  it('handles subscribe message', async () => {
    const response = await subscriptions({ text: '@newsbot subscribe' })
    expect(newsletter.subscribe).toHaveBeenCalled()
    expect(newsletter.unsubscribe).not.toHaveBeenCalled()
    expect(response.text).toContain('on the list')
  })

  it('handles unsubscribe message', async () => {
    const response = await subscriptions({
      text: '@newsbot please remove me from the list',
    })
    expect(newsletter.subscribe).not.toHaveBeenCalled()
    expect(newsletter.unsubscribe).toHaveBeenCalled()
    expect(response.text).toContain('to see you go')
  })

  it('handles unknown message', async () => {
    const response = await subscriptions({
      text: '@newsbot flarblegarblemarble?',
    })
    expect(response.text).toContain("didn't understand that")
  })
})
